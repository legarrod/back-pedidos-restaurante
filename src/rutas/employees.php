<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//get all customers
$app->get('/api/customers', function (Request $request, Response $response) {

    return try_catch_wrapper(function(){
        //throw new Exception('malo');
        $sql =  "SELECT *, DATEDIFF(NOW(), last_call) AS diference_days FROM `customers` WHERE state_customer = 1 ORDER BY diference_days DESC";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        
        foreach ($resultado as $i=>$registro)  {
            $resultado[$i]['id'] = (int)$registro['id'];  
            $resultado[$i]['diference_days'] = (int)$registro['diference_days'];
        }

        return $resultado ?: [];
    }, $response);
});


//get customer for dni or full name
$app->get('/api/customers/{cc}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $cc = $request->getAttribute('cc');
        $sql =  "SELECT * FROM `customers` WHERE identification_number LIKE '%{$cc}%' OR full_name LIKE '%{$cc}%'";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});
$app->get('/api/customersid/{id}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $id = $request->getAttribute('id');
        $sql =  "SELECT * FROM customers where id = {$id}";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

//create new customer
$app->post('/api/customers/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
          //throw new Exception('malo');
          $params = $request->getParams(); 
          
          function consultar($dni){
            $sql =  "SELECT * FROM customers where identification_number = $dni";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executeQuery($sql);
            return empty($resultado);
         }

         if (consultar($params['identification_number'])) {
            $sql = "INSERT INTO customers (id, document_type, identification_number, full_name, customer_type, address_customer, email, cellphone, creation_date, last_call, recurrence) VALUES 
            (NULL,:document_type,:identification_number,:full_name, :customer_type, :address_customer,:email,:cellphone,:creation_date,:last_call, :recurrence)";
            $dbConexion = new DBConexion(new Conexion());
         }
         
        $resultado = $dbConexion->executePrepare($sql, $params);
        return $resultado ?: [];
      }, $response);
  });

  $app->post('/api/customersall/post', function (Request $request, Response $response) {
    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
      $params = $request->getParams(); 

      function consultar($sku){
        $sql =  "SELECT * FROM customers where identification_number = $sku";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return empty($resultado);
     }

      foreach ($params as $key => $value) {
        if (consultar($value['identification_number'])) {
            $sql = "INSERT INTO customers (id, document_type, identification_number, full_name, customer_type, address_customer, email, cellphone, creation_date, last_call, state_customer, recurrence) VALUES 
            (NULL,:document_type,:identification_number,:full_name, :customer_type, :address_customer,:email,:cellphone,:creation_date,:last_call, :state_customer, :recurrence)";
            $dbConexion = new DBConexion(new Conexion());
            $resultado = $dbConexion->executePrepare($sql, $value);
        }else{

            $sql = "UPDATE customers SET 
            document_type = :document_type,
            identification_number = :identification_number,
            full_name = :full_name,
            customer_type = :customer_type,
            address_customer = :address_customer,
            email = :email,
            cellphone = :cellphone,
            creation_date = :creation_date,
            last_call = :last_call,
            state_customer = :state_customer,
            recurrence = :recurrence WHERE identification_number = :identification_number";
             $dbConexion = new DBConexion(new Conexion());
             $resultado = $dbConexion->executePrepare($sql, $value);
        }

      }

    return $resultado ?: [];
    }, $response);
  });

  //update all information for customer
$app->put('/api/customers/update', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE customers SET 
        identification_number = :identification_number,
        full_name = :full_name,
        address_customer = :address_customer,
        email = :email,
        cellphone = :cellphone,
        creation_date = :creation_date,
        recurrence = :recurrence WHERE identification_number = :identification_number";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

//update date of last call
$app->put('/api/customers/update/last-call', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE customers SET 
        id = :id,
        last_call = :last_call WHERE id = :id";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

//delete customers (change of statecustomer)
 $app->put('/api/customers/delete', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
         //throw new Exception('malo');
         $sql = "UPDATE customers SET 
         id = :id,
        state_customer = :state_customer WHERE id = :id";
         $dbConexion = new DBConexion(new Conexion());
        $params = $request->getParams(); 
        
         $resultado = $dbConexion->executePrepare($sql, $params);
         return $resultado ?: [];
     }, $response);
 });

$app->delete('/api/customers/delete/{dni}', function (Request $request, Response $response) {

    return try_catch_wrapper(function() use ($request){
        //throw new Exception('malo');
        $sql =  "DELETE FROM customers where dni = :dni";
        $dbConexion = new DBConexion(new Conexion());
        $resultado = $dbConexion->executeQuery($sql);
        return $resultado ?: [];
    }, $response);
});

?>